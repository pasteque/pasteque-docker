#!/bin/sh

docker build -t pasteque/pasteque-jsadmin js-admin/
docker build -t pasteque/pasteque:8.0-alpha4 api/
docker build -t pasteque/pasteque-admin:8.0.0 admin/
