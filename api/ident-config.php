<?php
//    Pastèque Web back office
//
//    Copyright (C) 2013 Scil (http://scil.coop)
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

// Base path to the directory containing all user's ini files.
// Either relative to the base directory of Pasteque (start with ./)
// or an absolute path.
// Each ini file is named <login>_id.ini, some special characters are
// replaced by 'underscore charcode underscore'
//
// Ini files contains:
// pwd_hash=<password hash>
$config['path'] = '/etc/pasteque';
