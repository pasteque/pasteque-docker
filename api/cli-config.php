<?php
namespace Pasteque\Server;
use \Pasteque\Server\System\DAO\DoctrineDAO;

$cfg = parse_ini_file(__DIR__ . '/config.ini');
$dbInfo = ['type' => $cfg['database/static/type'], 'host' => $cfg['database/static/host'],
    'port' => $cfg['database/static/port'], 'name' => $cfg['database/static/name'],
    'user' => $cfg['database/static/user'], 'password' => $cfg['database/static/password']];
$dao = new DoctrineDAO($dbInfo, ['debug' => false]);
return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($dao->getEntityManager());
