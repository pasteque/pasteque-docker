#!/bin/sh
# Pasteque Server
##########################

cd /var/www/html
php vendor/bin/doctrine orm:schema-tool:update --force
php vendor/bin/doctrine orm:generate-proxies
