<!-- Public domain -->
<html dir="ltr" lang="en-US">
    <head>
        <meta charset="UTF-8" />
        <title>password_hash</title>
        <style type="text/css">
         pre { border: 1px solid #444; padding: 1ex; }
        </style>
    </head>
    <body>
        <h1>Password hash generator</h1>
        <form action="user_ini.php" method="POST" onsubmit="javascript:sendpwd();">
        <p>Enter password here</p>
        <p><input type="password" name="password" placeholder="password to hash" /></p>
        <p><input type="submit" value="send" /></p>
        </form>
        <h2>Your &lt;user&gt;_id.ini file</h2>
        <pre><?php
if (array_key_exists('password', $_POST)) {
    echo htmlspecialchars("password=" . password_hash($_POST['password'], PASSWORD_DEFAULT));
} else {
    echo "Empty, use the form above";
}?></pre>
        <h2>Server source code</h2>
             <p>This is the php code running this page and filling the &lt;user&gt;_id.ini block.</p>
        <pre>if (array_key_exists('password', $_POST)) {
    echo htmlspecialchars("password=" . password_hash($_POST['password'], PASSWORD_DEFAULT));
} else {
    echo "Empty, use the form above";
}</pre>
        <h2>Standalone script</h2>
        <p>Call with php-cli, enter the password twice, outputs the hash. Usage:</p>
        <pre>php hasher.php > login_id.ini</pre>
<pre>&lt;?php
if ($argc < 2) {
    system("stty -echo");
    $password = substr(fread(STDIN, 80), 0, -1);
    $password2 = substr(fread(STDIN, 80), 0, -1);
    system("stty echo");
    if ($password != $password2) {
        file_put_contents("php://stderr", "Password mismatch\n");
        die();
    }
} else {
    $password = $argv[1];
}
echo "password=" . password_hash($password, PASSWORD_DEFAULT);</pre>
    <body>
</html>
