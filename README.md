# Pasteque Server
> https://www.pasteque.org

Presentation
============
Pasteque Server is a POS (point of sale) project under the [GNU Affero General Public License v3][gnu].
Docker files for Pasteque POS, using PostgreSQL as database engine

Build all images with build_all_images.sh script

For a basic test usecase in local use docker-compose (Docker-compose 1.11 or superior required)
, add following lines to your /etc/hosts file :

127.0.0.1 pasteque.docker
127.0.0.1 pastequejs.docker
127.0.0.1 pastequeapi.docker

All users and passwords are set to 'pasteque'

The hostnames pasteque*.docker can be changed in docker-compose.yml file by editing the VIRTUAL_HOST environment variables