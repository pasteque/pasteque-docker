#!/bin/bash
set -e


echo "Creating user $DB_USER with password $DB_USER_PASSWORD and database $DB_NAME with all privileges given to $DB_USER"

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
    CREATE ROLE $DB_USER WITH PASSWORD '$DB_USER_PASSWORD' ;
    ALTER ROLE $DB_USER WITH LOGIN;
    CREATE DATABASE $DB_NAME;
    GRANT ALL PRIVILEGES ON DATABASE $DB_NAME TO $DB_USER;
EOSQL
